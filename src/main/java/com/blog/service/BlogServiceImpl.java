package com.blog.service;

import java.util.List;

import com.blog.exceptions.BlogNotFoundException;
import com.blog.exceptions.IdNotFoundException;
import com.blog.model.Blog;
import com.blog.repository.BlogRepositoryImpl;
import com.blog.repository.IBlogRepository;

import org.bson.Document;

public class BlogServiceImpl implements IBlogService {

    IBlogRepository methods = new BlogRepositoryImpl();

    @Override
    public void addingBlog(Blog blog) {
        methods.addBlog(blog);
    }

    @Override
    public void deletingBlog(int id) {
        methods.deleteBlog(id);
    }

    @Override
    public void updatingBlog(int id, String name) {
        methods.updateBlog(id, name);
    }

    @Override
    public List<Blog> getAllBlogs() {
        List<Blog> allBlogs = null;
        try {
            allBlogs = methods.findAllBlogs();
        } catch (BlogNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return allBlogs;
    }

    @Override
    public List<Blog> getAllBlogsByAuthor(String author) {
        List<Blog> allBlogsByAuthor = null;
        try {
            allBlogsByAuthor = methods.findAllBlogs();
        } catch (BlogNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return  allBlogsByAuthor;
    }

    @Override
    public List<Blog> getByLikes(int likes) {
        List<Blog> allBlogs = null;
        try {
            allBlogs = methods.findAllBlogs();
        } catch (BlogNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return allBlogs;
    }

    @Override
    public Blog getBlogById(int id) {
        Blog blog = null;
        try {
            blog = methods.findBlogById(id);
        } catch (IdNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return blog;
    }
    
}
