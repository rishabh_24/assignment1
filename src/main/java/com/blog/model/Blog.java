package com.blog.model;

import org.bson.codecs.pojo.annotations.BsonProperty;

public class Blog {
    @BsonProperty(value = "_id")
    private int blogId;
    @BsonProperty(value = "author")
    private String blogAuthor;
    @BsonProperty(value = "content")
    private String blogContent;
    private int likes;
    
    public Blog() {
        super();
    }

    public Blog(int blogId, String blogAuthor, String blogContent, int likes) {
        this.blogId = blogId;
        this.blogAuthor = blogAuthor;
        this.blogContent = blogContent;
        this.likes = likes;
    }

    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public String getBlogAuthor() {
        return blogAuthor;
    }

    public void setBlogAuthor(String blogAuthor) {
        this.blogAuthor = blogAuthor;
    }

    public String getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(String blogContent) {
        this.blogContent = blogContent;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    @Override
    public String toString() {
        return "Blog{" + "\n" +
                "\tblogId :" + blogId + ",\n" +
                "\tblogAuthor :'" + blogAuthor + ",\n" +
                "\tblogContent :'" + blogContent + ",\n" +
                "\tlikes :" + likes + "\n" +
                '}' + "\n\n";
    }
}
