package com.blog.helper;

import com.blog.model.Blog;
import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;


public class HelperFunction {
    public static Blog blogGenerator(int id) {
        Integer likes = (int) (1000 * Math.random());
        Lorem lorem = LoremIpsum.getInstance();
        int a = (int)(2 * Math.random());
        String content = lorem.getParagraphs(a, 3);
        return new Blog(id, lorem.getName(), content, likes);
    }
}
