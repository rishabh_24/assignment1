package com.blog.main;

import com.blog.dbconnection.DbManager;
import com.blog.exceptions.BlogNotFoundException;
import com.blog.exceptions.IdNotFoundException;
import com.blog.helper.HelperFunction;
import com.blog.service.BlogServiceImpl;
import com.blog.service.IBlogService;
import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;

public class Driver {
    public static Lorem lorem = LoremIpsum.getInstance();
    public static void main(String[] args)  {
        IBlogService service = new BlogServiceImpl();
        DbManager.opnConnection();
        System.out.println("Connection has been opened!!");
        
        System.out.println("Adding blogs to database...\n==========================\n");
        for(int i = 0; i < 1e3; i++) {
            service.addingBlog(HelperFunction.blogGenerator(i + 1));
        }

        System.out.println("\nReading blogs from database...\n==========================\n");
        try {
            service.getAllBlogs().stream().forEach(System.out::println);
        } catch (BlogNotFoundException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("\nUpdating blog from database...\n==========================\n");
        service.updatingBlog(1 + (int)(1e3 * Math.random()), lorem.getName());

        System.out.println("\nDeleting blog from database...\n==========================\n");
        service.deletingBlog((int)(1e3 * Math.random()));

        String author = lorem.getName();
        System.out.println("\nFinding blogs from database with specific author " + author+" \n==========================\n");
        try {
            service.getAllBlogsByAuthor(author).stream().forEach(System.out::println);
        } catch (BlogNotFoundException e) {
            System.out.println(e.getMessage());
        }

        int likes = (int)(1e3 * Math.random());
        System.out.println("\nFinding blogs from database with likes greater than " + likes +" \n==========================\n");
        try {
            service.getByLikes(likes).stream().forEach(System.out::println);
        } catch (BlogNotFoundException e) {
            System.out.println(e.getMessage());
        }

        int id = (int)(1e3 * Math.random());
        System.out.println("\nFinding blog from database with specific id " + id+" \n==========================\n");
        try {
            System.out.println(service.getBlogById(id));
        } catch (IdNotFoundException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("Closing connection...");
        DbManager.closeConnection();
    }
}
