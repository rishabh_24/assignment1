package com.blog.repository;

import java.util.List;

import com.blog.exceptions.BlogNotFoundException;
import com.blog.exceptions.IdNotFoundException;
import com.blog.model.Blog;


public interface IBlogRepository {
    void addBlog(Blog blog);
    void deleteBlog(int id);
    void updateBlog(int id, String name);
    List<Blog> findAllBlogs() throws BlogNotFoundException;
    List<Blog> findAllBlogsByAuthor(String author) throws BlogNotFoundException;
    List<Blog> findByLikes(int likes) throws BlogNotFoundException;
    Blog findBlogById(int id) throws IdNotFoundException;
}
