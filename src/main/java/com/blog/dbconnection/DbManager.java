package com.blog.dbconnection;



import com.blog.model.Blog;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class DbManager {
    static MongoClient mongoInstance;
	
	public static void opnConnection() {
		String connectionString = "mongodb://localhost:27017";
        ConnectionString connection = new ConnectionString(connectionString);
        CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
        CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
        // set the codecregistry and the connection string
        MongoClientSettings clientSettings = MongoClientSettings.builder().applyConnectionString(connection)
                .codecRegistry(codecRegistry).build(); // configuration for mongodb connection
        mongoInstance = MongoClients.create(clientSettings);
	}
	
	public static void closeConnection() {
		mongoInstance.close();
	}
	
	public MongoDatabase getDatabase() {
		MongoDatabase database = mongoInstance.getDatabase("kloud");
		return database;
	}
	
	public MongoCollection<Blog> getCollection(){
		MongoDatabase database = getDatabase();
		MongoCollection<Blog> document = database.getCollection("blog",Blog.class);
		return document;
	}
}
