package com.blog.repository;

import java.util.ArrayList;
import java.util.List;

import com.blog.dbconnection.DbManager;
import com.blog.helper.HelperFunction;
import com.blog.model.Blog;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import org.bson.conversions.Bson;

public class BlogRepositoryImpl implements IBlogRepository{
    DbManager manager = new DbManager();
    @Override
    public void addBlog(Blog blog) {
        MongoCollection<Blog> collection = manager.getCollection();
        collection.insertOne(blog);
        // System.out.println("Blog has been added successfully!!");
    }
    @Override
    public void deleteBlog(int id) {
        MongoCollection<Blog> collection = manager.getCollection();
        Bson filter = Filters.eq("_id", id);
        collection.deleteOne(filter);
        System.out.println("Blog with " +(id)+ " been deleted successfully!!");
    }
    @Override
    public void updateBlog(int id, String name) {
        MongoCollection<Blog> collection = manager.getCollection();
        Bson filter = Filters.eq("_id", id);
        Bson update = Updates.set("author", name);
        System.out.println(name);
        // collection.updateOne(filter, update);
        collection.findOneAndUpdate(filter, update);
        System.out.println("Blog with " +(id)+ " been updated successfully!!");
    }
    @Override
    public List<Blog> findAllBlogs() {
        MongoCollection<Blog> collection = manager.getCollection();
        return collection.find().into(new ArrayList<>());
    }
    @Override
    public List<Blog> findAllBlogsByAuthor(String author) {
        MongoCollection<Blog> collection = manager.getCollection();
        Bson filter = Filters.eq("author", author);
        return collection.find(filter).into(new ArrayList<>());
    }
    @Override
    public List<Blog> findByLikes(int likes) {
        MongoCollection<Blog> collection = manager.getCollection();
        Bson filter_1 = Filters.eq("likes", likes);
        Bson filter_2 = Filters.eq("$gte", 300);
        Bson filter = Filters.and(filter_1, filter_2);
        return collection.find(filter).into(new ArrayList<>());
    }
    @Override
    public Blog findBlogById(int id) {
        MongoCollection<Blog> collection = manager.getCollection();
        return  collection.find(Filters.eq("_id", id)).first();
    }

   
    
}
