package com.blog.service;

import java.util.List;

import com.blog.exceptions.BlogNotFoundException;
import com.blog.exceptions.IdNotFoundException;
import com.blog.model.Blog;


public interface IBlogService {
    void addingBlog(Blog blog);
    void deletingBlog(int id);
    void updatingBlog(int id, String name);
    List<Blog> getAllBlogs() throws BlogNotFoundException;
    List<Blog> getAllBlogsByAuthor(String author) throws BlogNotFoundException;
    List<Blog> getByLikes(int likes) throws BlogNotFoundException;
    Blog getBlogById(int id) throws IdNotFoundException;
}
