package com.blog.exceptions;

public class BlogNotFoundException extends Exception {

    public BlogNotFoundException() {
    }

    public BlogNotFoundException(String message) {
        super(message);
    }
    
}
